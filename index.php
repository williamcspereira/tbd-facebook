<?php
session_start();
set_time_limit(0);

$servidor = 'localhost';
$usuario = 'root';
$senha = '123';
$banco = 'tbdfacebook';

// Conecta-se ao banco de dados MySQL
global $mysqli;
$mysqli = new mysqli($servidor, $usuario, $senha, $banco);
if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());
$mysqli->set_charset("utf8");


// Facebook SDK
require 'facebook-php-sdk-v4-5.0.0/src/Facebook/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '1248698231827862',
  'app_secret' => '1f09c14e91cc84b6d863c106ac3cdd80',
  'default_graph_version' => 'v2.7',
  ]);

// Pegar AccessToken no https://developers.facebook.com/tools/explorer/ - não esquecer de selecionar o aplicativo correto
$accessToken = 'EAARvrzaKLZAYBAAN5txcPMbWWlhIQFmERub9BZBP6N9rn7sSYwKU5ydjh7plggLHC7GRiI0SEEN4IwGEItMmmNp86OtIcfVQP0c3xLH2kdDqF1ZAbo8zNP7ZAo3ZB1m67efpo88JhE4P7KGGxLRK0BhWijADOrypfCqBmv85T1CbBsewU4g0P';

$fb->setDefaultAccessToken($accessToken);

// Cadastrar as escolas inicialmente no banco de dados
// getSchoolsPageId('escolas.csv',$mysqli);

// Pegar likes das páginas das escolas
// $sql = "SELECT page_id FROM `page` WHERE page_id NOT IN (SELECT page_id FROM page_searched) AND type = 0 LIMIT 1;";
// $query = $mysqli->query($sql);
// while ($dados = $query->fetch_array()) {
// 	echo $dados['page_id'];
// 	getReactionsFromPage($dados['page_id'],$mysqli);
// 	$sql1 = "INSERT INTO `page_searched`(`page_id`) VALUES ('{$dados['page_id']}')";
// 	$mysqli->query($sql1);
// 	echo("<meta http-equiv='refresh' content='1'>");
// }

//Coletar comentários de posts das paginas 
// $sql = "SELECT object_id FROM `object` WHERE object_id NOT IN (SELECT object_id FROM object_searched) LIMIT 1;";
// $query = $mysqli->query($sql);
// while ($dados = $query->fetch_array()) {
// 	echo $dados['object_id'];
// 	getCommentsFromObject($dados['object_id'],$mysqli);
// 	$sql1 = "INSERT INTO `object_searched`(`object_id`) VALUES ('{$dados['object_id']}')";
// 	$mysqli->query($sql1);
// 	echo("<meta http-equiv='refresh' content='1'>");
// }

//Método para pegar comentários de um post
function getCommentsFromObject($objectId, $mysqli) {
	$result01 = fbRequest($objectId . '/comments?limit=100');
	var_dump($result01);
	foreach($result01 as $result) {
		$sql = "SELECT fb_id from user where fb_id = " . $result['from']['id'];
		$temp = $mysqli->query($sql);
		if($temp->num_rows == 0) {
			$sql1 = "INSERT INTO `user`(`fb_id`) VALUES ('{$result['from']['id']}')";
			$mysqli->query($sql1) or trigger_error("Query Failed! Error: ". mysqli_error($mysqli));
		}
		$sql1 = "INSERT INTO `comment`(`comment_id`,`object_id`,`message`,`user_id`) 
			VALUES ('{$result['id']}','{$objectId}','{$result['message']}','{$result['from']['id']}')";
		$mysqli->query($sql1) or trigger_error("Query Failed! Error: ". mysqli_error($mysqli));
	}
}

// Método para fazer solicitação ao Facebook
function fbRequest($request) {
	global $fb;
	try {
		$profile_request = $fb->get($request)->getGraphEdge();
		$res = array();
		while($profile_request) {
			$res = array_merge($res, $profile_request->asArray());
			$profile_request = $fb->next($profile_request);
		}
		return $res;

	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// Quando a Graph API retorna erro
		$response = array();
		$response['error'] = 'Graph returned an error: ' . $e->getMessage();
		die();
		session_destroy();
		return $response;
	}
}

/* 	Método para pegar id's das páginas das escolas, no Facebook
	Informar caminho do arquivo .csv com a lista de escolas */
// function getSchoolsPageId($schoolsListFile, $mysqli) {
// 	// Palavras chave
// 	$keywords = array(
// 		'Escola Estadual',
// 		'E.E.',
// 		'E.E',
// 		'Escola',
// 		'Colegio',
// 		'Colégio',
// 		'CESEC'
// 	);
// 	// "Stopwords" (para não pegar por exemplo, auto escolas)
// 	$stopwords = array(
// 		'auto'
// 	);
// 	// Lista das escolas
// 	$escolas = csvToArray($schoolsListFile);
// 	foreach ($escolas as $value) {
// 	    $results = fbRequest('search?q=' . $value[0] . '&type=page&limit=100&since=&until=');
// 		foreach ($results as $result) {
// 			if(contains($keywords,$result['name'])&&!contains($stopwords,$result['name'])) {
// 				$sql = "INSERT INTO .`page` (`page_id`,`page_name`,`schoolname`) VALUES (\"{$result['id']}\", \"{$result['name']}\", \"{$value[0]}\")";
// 				$mysqli->query($sql);
// 			}
// 		}
// 	}
// }

// Método para pegar lista de usuários que deram like em uma páginas
function getReactionsFromPage($pageId, $mysqli) {
	$result01 = fbRequest($pageId . '/feed?limit=100&since=1441065660&until=1472774340');
	$result02 = array();
	foreach($result01 as $result) {
		$result03 = fbRequest($result['id'] . '/reactions?limit=100');
		foreach($result03 as $user) {
			$user['object_id'] = $result['id'];
			$result02[] = $user;
		}
	}	
	foreach($result02 as $user) {
		// insert user if not exists, insert reaction.

		$sql = "INSERT IGNORE INTO `object` (`object_id`, `page_id`) VALUES (\"{$user['object_id']}\",\"{$pageId}\");";
		$mysqli->query($sql) or trigger_error("Query Failed! Error: ". mysqli_error($mysqli), E_USER_ERROR);

		$sql = "INSERT IGNORE INTO `user` (`fb_id`) VALUES (\"{$user['id']}\");";
		$mysqli->query($sql) or trigger_error("Query Failed! Error: ". mysqli_error($mysqli), E_USER_ERROR);

		$sql = "INSERT IGNORE INTO `reaction` (`object_id`,`user_id`,`type`) VALUES (\"{$user['object_id']}\",\"{$user['id']}\",\"{$user['type']}\");";
		$mysqli->query($sql) or trigger_error("Query Failed! Error: ". mysqli_error($mysqli), E_USER_ERROR);
	}
}

function userExistsInArray($usersList, $userId) {
	foreach($usersList as $user) {
		if($user['id'] == $userId) {
			return true;
		}
	}
	return false;
}

// Método para converter arquivo csv em array
function csvToArray($csvFile) {
	$file = fopen($csvFile, 'r');
	$list = array();
	while (($line = fgetcsv($file)) !== FALSE) {
		$list[] = $line;
	}
	fclose($file);
	return $list;
}

// Método para verificar se uma lista de palavras contem em uma frase
function contains(array $words, $text) {
	foreach ($words as $word) {
		if (strpos($text, $word) !== FALSE) { 
			return true;
		}
	}
	return false;
}


 ?>
